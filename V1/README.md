# Izvještaj

# Opis izrade:

- Prvi inicijal mojeg imena sam napravio prateći korake u
    uputama (koristeći naredbu extrude na licima kocke te
    manipulacijom njenih dimenzija)
- Slovo „Đ“ u sebi ima zaobljeni dio pa sam napravio posebni
    kvadar te pomoću opcije loop cut and slide u edit mode-u
    podijelio ga na više djelova
- Zatim sam stvorio object empty i pomaknuo ga pokraj sredine
    kvadra
- Kvadru sam dodao modifier simple deform te odabrao opciju
    bend i za centar zakrivljenja postavio objekt empty kako bi se
    kvadar zakrivio oko njega te napravio luk
- Ostatak slova je napravljen slično kao i kod slova „M“


# Opis izrade:

- Stol se sastoji od tankog cilindra koji predstavlja vrh, cilindra
    koji je glavna i jedina noga stola te pomoćnih držača koji su
    zapravo razdužene stranice najnižeg cilindra
- Stolica je također tanki cilindar kojemu su 4 stranice izdužene i
    predstavljaju noge
- Dvije stražnje noge izdužene su i prema gore kako bi držale
    naslon stolice
- Naslon je kao i kod luka u slovu „Đ“ napravljen pomoću
    zakrivljenja oko objekta te nekim izmjenama u veličini i poziciji
    kako bi se bolje slagalo sa stolicom



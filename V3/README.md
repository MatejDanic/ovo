# Izvještaj

Postupak iscrtavanja:

- Iz svake točke ekrana šaljemo zraku koja kroz tu točku ulazi u scenu
- Tražimo najbliži presjek te zrake sa predmetima u sceni
- Ako neka presjeka vraćamo boju pozadine
- Ako se presjek nađe, računamo osvjetljenje u toj točki i reflektiranu i/ili refraktiranu zraku
- U svakom trenutku ispitujemo da li je zraka prešla granicu dubine rekurzije te ako je vraćamo
    crnu boju
- Za računanje osvjetljenja koristi se Phongov model odbijanja svjetlosti, koji se sastoji od
    ambijentne, difuzne i spekularne komponente
- Intenziteti svjetlosti zapisani su kao vektori boje koji sadrže RGB (Red, Green i Blue)
    komponente i poprimaju vrijednosti od 0 do 1
- Kako bi postigli efekt sjene, koristimo zrake koje idu od mjesta presjeka prema izvoru
    svjetlosti te ako im se na putu nalazi predmet, na tom mjestu se nalazi sjena te
    zanemarujemo difuzne i spekularne komponente
- Konačan rezultat obuhvaća zvbroj svih komponenti osvjetljenja, reflektirane i reffraktirane
    zrake svjetlosti

Slika 1 Slika siluete scene (samo presjek)


Slika 2 Slika s ambijentnom komponentom

Slika 3 Slika s ambijentnom i difuznom komponentom


Slika 4 Slika s ambijentnom, difuznom i spekularnom komponentom (puno lokalno osvjetljenje)

Slika 5 Slika s lokalnim osjvetljenjem i sjenama


Slika 6 Slika s lokalnim osvjetljenjem, sjenama i reflektiranim zrakama

Slika 7 Potpuna slika: lokalno osvjetljenje, refleksija, refrakcija i sjene


Slika 8 _Slika kugli s prvim slovom mojeg prezimena (Đ)_



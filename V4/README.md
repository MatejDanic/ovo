# Izvještaj

2. Zadatak:

```
A. Pokretanjem ovog programa otvara se prozor dimenzija 400x400 na poziciji 100, 100. U
sredini prozora nalazi se iscrtana siva kugla.
B. Dobiveni objekt se iscrtava metodom drawSphere. Metodi se predaju parametar 2.0 koji
označava radijus kugle i 0.01 koji označava korak povećanja kuta.
C. Dojam 3-dimenzionalnosti pri iscrtavanju objekta dobiva se zbog načina osvjetljenja objekta.
Naime, objekt je svjetliji u sredini, a prema krajevima je tamniji što daje dojam o blizini i
daljini dijelova kugle.
D. Varijabla step u metodi drawSphere predstavlja korak povećanja kuta u svakoj iteraciji petlje
koja iscrtava kuglu. Ako se taj broj poveća kugla poprima dosta oštriji oblik i mogu se vidjeti
pojedine kružnice koje ju sačinjavaju. Što je manji to je kugla detaljnija (preciznija).
```
3. Zadatak:
    - U ovom zadatku trebalo je nadopuniti tri funkcije: drawSphere, spinDisplay i display.
       DrawSphere funkcija identična je onoj iz drugog zadatka. SpinDisplay funkcija izracunava
       pomake rotacije zemlje oko svoje osi te oko sunca i rotacije mjeseca oko svoje osi i oko
       zemlje. Funkcija display iscrtava dijelove Sunčevog sustava. Za radijuse Sunca, Zemlje i
       Mjeseca korišteni su parametri iz VRML laboratorijske vježbe.
    - Za dodavanje boja elementima koristi se funkcija glCallList te joj se predaju konstantni
       parametri YELLOWMAT, BLUEMAT ili WHITEMAT ovisno o pojedinom elementu.



# Izvještaj

## PRVI ZADATAK

- Nakon upoznavanja sa osnovama VRML-a i njegovom sintaksom postupkom pokušaja i
    pogreške redom sam stvarao kugle te ih skalirao, translatirao i rotirao kako bi izradio dijelove
    aviona koji mu nedostaju
- Nakon toga trupu aviona sam dodao teksturu upisivanjem sljedeceg koda u Appearance
    odlomak:
    _texture ImageTexture {_
       _url "trup.jpg"_
    _}_
- _Na samom kraju dodao sam svoje inicijale (M Đ) na desnom krilu avionu čvorom text te_
    _skalirao i rotirao za željeni položaj_

**DRUGI ZADATAK**

- Sunce, Zemlju i Mjesec napravio sam kreirajući 3 kugle sa odgovarajućim zadanim radijusima
    koji su skalirani za lakši prikaz
- Upisao sam njihove položaje stavljajući sunce u sredinu, a zemlju i mjesec udaljene za
    zadanu udaljenost
- Za svako tijelo odredio sam boje u RGB formatu koje su zadane u dokumentu sa uputama
- Animacija je stvorena identicno kao u animiranaKocka.wrl za cijeli sunčev sustav kako bi
    dobili rotaciju Zemlje (skupa s Mjesecom) oko Sunca
- Jedina promjena je bila u vremenu za jednu rotaciju koja je postavljena na zadani iznos (
    sekundi)
- Taj postupak je ponovljen za rotaciju Mjeseca oko Zemlje

**TREĆI ZADATAK**

- Teksture tijela potražio sam na google image search-u te ih stavio u datoteku gdje se nalaze
    wrl datoteke
- Datoteci iz drugog zadatka sam dodao animacije pojedinih tijela s odgovarajućim vremenima
    kako bi postigao rotaciju oko respektivnih osi


